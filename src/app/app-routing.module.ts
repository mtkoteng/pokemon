import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutes} from './shared/enums/app-routes.enum';
import {SessionGuard} from './guards/session.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import ('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: AppRoutes.Trainer,
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule),
    canActivate: [ SessionGuard ]
  },
  {
    path: AppRoutes.Catalogue,
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [ SessionGuard ]
  },
  {
    path: AppRoutes.Detail,
    loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule),
    canActivate: [ SessionGuard ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
