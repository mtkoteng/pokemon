import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {PokemonFacade} from '../../pokemon/pokemon.facade';
import {Router} from '@angular/router';
import {AppRoutes} from '../../shared/enums/app-routes.enum';
import {Pokemon} from '../../pokemon/models/pokemon.model';
import {PokemonState} from '../../pokemon/state/pokemon.state';
import {Observable, Subscription} from 'rxjs';
import {AppStorage} from '../../utils/app-storage';

@Component({
  selector: 'app-catalogue',
  templateUrl: 'catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit{
  // variable to keep track of the current page user is
  private page = Number(AppStorage.get('count')) | 1;

  constructor(private readonly pokemonFacade: PokemonFacade, private readonly router: Router, private readonly pokemonState: PokemonState) {
    AppStorage.set('count', 1);
  }
  // OnInit that gets the current page(pagination)
  ngOnInit(): void {
    this.pokemonFacade.getPokemonsPagination(this.page);
  }
  // Get pokemons to list out
  get pokemons$(): Observable<Pokemon[]>{
    return this.pokemonFacade.pokemons$();
  }
  // Collects pokemon and adds them to trainer
  public collectPokemon(pokemon: Pokemon): Pokemon[] {
    let pokemonsCollected: Pokemon[] = AppStorage.get('collected');
    if (AppStorage.get('collected') === null){
      pokemonsCollected = [];
    }
    pokemonsCollected.push(pokemon);
    AppStorage.set('collected' , pokemonsCollected);
    alert(`${pokemon.name} was collected!`);
    return pokemonsCollected;
  }
  // Function that gets next page of Pokemons.
  public next(): void {
    // if test that checks that there are still more pages left to render
    if (this.page < 56)
    {
      this.page += 1;
      AppStorage.set('count', this.page);
      this.pokemonFacade.getPokemonsPagination(this.page);
    }
    else{
      alert('No Pokemons left to display');
    }
  }
  // Function that goes back one page.
  public prev(): void {
    if (this.page === 1)
    {
      alert('Already on first page');
    }else{
      this.page = this.page - 1;
      AppStorage.set('count', this.page);
      this.pokemonFacade.getPokemonsPagination(this.page);
    }
  }

}
