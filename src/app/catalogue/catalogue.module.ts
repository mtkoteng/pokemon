import {NgModule} from '@angular/core';
import {CataloguePage} from './pages/catalogue/catalogue.page';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import {CatalogueComponent} from './components/catalogue.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    CataloguePage,
    CatalogueComponent
  ],
  imports: [
    CatalogueRoutingModule,
    CommonModule
  ]
})
export class CatalogueModule {}
