import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CataloguePage} from './pages/catalogue/catalogue.page';
import {DetailPage} from '../detail/pages/detail/detail.page';

const routes: Routes = [
  {
    path: '',
    component: CataloguePage
  },
  {
    path: ':name',
    component: DetailPage
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CatalogueRoutingModule{}
