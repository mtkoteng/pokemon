import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailPage} from './pages/detail/detail.page';

// Routing module for detail
const routes: Routes = [
  {
    path: '',
    component: DetailPage
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DetailRoutingModule{}
