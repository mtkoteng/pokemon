import {NgModule} from '@angular/core';
import {DetailPage} from './pages/detail/detail.page';
import {DetailRoutingModule} from './detail-routing.module';
import {DetailComponent} from './components/detail.component';
import {CommonModule} from '@angular/common';
import {PokemonProfileHeaderComponent} from './components/pokemon-profile-header/pokemon-profile-header.component';

// Module for detail
@NgModule({
  declarations: [
    DetailPage,
    DetailComponent,
    PokemonProfileHeaderComponent
  ],
  imports: [
    DetailRoutingModule,
    CommonModule,
  ]
})
export class DetailModule {}
