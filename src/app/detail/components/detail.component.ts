import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PokemonApi} from '../../pokemon/api/pokemon.api';
import {Pokemon} from '../../pokemon/models/pokemon.model';
import {AppStorage} from '../../utils/app-storage';

@Component({
  selector: 'app-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit{

  private readonly pokemonName: string = '';
  // Constructor that extracts name from routeparam and assigns it to pokemonName variable
  constructor(private readonly route: ActivatedRoute, private readonly pokemonApi: PokemonApi) {
    this.pokemonName = this.route.snapshot.paramMap.get('name');
  }
  // fetch api on init
  ngOnInit(): void{
    this.pokemonApi.getPokemon$(this.pokemonName);
  }
  // Get a specific Pokemon
  get pokemon(): Pokemon{
    return this.pokemonApi.pokemon;
  }
  // Adds pokemon objects to AppStorage so that you can list these out in pokemon collected in trainer page.
  public collectPokemon(pokemon: Pokemon): Pokemon[] {
    let pokemonsCollected: Pokemon[] = AppStorage.get('collected');
    if (AppStorage.get('collected') === null){
      pokemonsCollected = [];
    }
    pokemonsCollected.push(pokemon);
    AppStorage.set('collected' , pokemonsCollected);
    alert(`${pokemon.name} was collected!`);
    return pokemonsCollected;
  }
}
