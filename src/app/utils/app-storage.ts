export class AppStorage {
  public static set(key: string, value: any): void {
    const json = JSON.stringify(value);
    try {
      const encoded = btoa(json);
      localStorage.setItem(key, encoded);
    } catch (e) {
      console.error(e);
    }
  }
  public static get<T>(key: string): T | null {
    const stored = localStorage.getItem(key);
    if (!stored) {
      return null;
    }
    try {
      const decoded = atob(stored);
      const value = JSON.parse(decoded);
      return value as T;
    } catch (e) {
      return null;
    }
  }
}
