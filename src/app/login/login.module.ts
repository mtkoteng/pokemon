import {NgModule} from '@angular/core';
import {LoginPage} from './pages/login-page/login.page';
import {LoginRoutingModule} from './login-routing.module';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {FormsModule} from '@angular/forms';

// Login module
@NgModule({
  declarations: [
    LoginPage,
    LoginFormComponent
  ],
  imports: [
    LoginRoutingModule,
    FormsModule
  ],
})
export class LoginModule{}
