import {Component} from '@angular/core';
import {SessionService} from '../../../shared/service/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage{
  constructor(private readonly sessionService: SessionService, private readonly router: Router) {
    // Checks if session exists
    if (sessionService.active()){
      router.navigateByUrl('/catalogue');
    }
  }
}
