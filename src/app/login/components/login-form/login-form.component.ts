import {Component} from '@angular/core';
import {PokemonFacade} from '../../../pokemon/pokemon.facade';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {AppStorage} from '../../../utils/app-storage';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent{
  public username: string = '';

  constructor(private readonly fetchFacade: PokemonFacade, private readonly router: Router) {
  }

  get error$(): Observable<string> {
    return this.fetchFacade.error$();
  }

  // When log in, this method gets called. Stores the username in localStorage
  public onEnterClick(): Promise<boolean> {
    AppStorage.set('username', this.username);
    return this.router.navigate([AppRoutes.Trainer]);
  }
}
