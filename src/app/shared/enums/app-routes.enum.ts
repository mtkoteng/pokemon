export enum AppRoutes {
  // Enums for routes
  Login= 'login',
  Trainer= 'trainer',
  Detail= 'detail',
  Catalogue= 'catalogue'
}
