import {Injectable} from '@angular/core';
import {AppStorage} from '../../utils/app-storage';

@Injectable({
  providedIn: 'root'
})
export class SessionService{
  active(): boolean {
    const trainer = AppStorage.get('username');
    return Boolean(trainer);
  }
}
