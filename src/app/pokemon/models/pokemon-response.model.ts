import {Pokemon} from './pokemon.model';

export interface PokemonResponseModel{
  count: number;
  next: string;
  previous: string;
  results: Pokemon[];
}
