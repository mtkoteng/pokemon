export interface Pokemon {
  id?: number;
  name: string;
  image?: string;
  url: string;
  abilities?: any[];                // may give all any's own interfaces
  baseExperience?: number;
  forms?: any[];
  gameIndices?: any[];
  height?: number;
  weight?: number;
  moves?: any[];
  sprites?: any[];
  stats?: PokemonStat[];
  types?: PokemonType[];
}

export interface PokemonSprite {
  back_shiny: string;
  front_shiny: string;
  other: PokemonSpriteOther;
}

export interface PokemonSpriteOther {
  dream_world: any;
  'official-artwork': PokemonSpriteOfficial;
}

export interface PokemonSpriteOfficial{
  front_default: string;
}

export interface PokemonType {
  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType {
  name: string;
  url: string;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface PokemonStatStat {
  name: string;
  url: string;
}
