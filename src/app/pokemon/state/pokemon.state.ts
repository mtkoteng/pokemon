import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Pokemon} from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonState {
  // Handles the states when fetching pokemons.
  private readonly error$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private readonly pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject<Pokemon[]>([]);

  public getError$(): Observable<string> {
    return this.error$.asObservable();
  }
  public setError(error: string): void {
    this.error$.next(error);
  }
  public getPokemons$(): Observable<Pokemon[]> {
    return this.pokemons$.asObservable();
  }
  public setPokemons$(pokemons: Pokemon[]): void {
    this.pokemons$.next(pokemons);
  }
}
