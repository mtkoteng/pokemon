import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {PokemonApi} from './api/pokemon.api';
import {Pokemon} from './models/pokemon.model';
import {map} from 'rxjs/operators';
import {PokemonState} from './state/pokemon.state';
import {Observable} from 'rxjs';
import {PokemonResponseModel} from './models/pokemon-response.model';

@Injectable({
  providedIn: 'root'
})

export class PokemonFacade {
  constructor(private readonly fetchApi: PokemonApi, private readonly pokemonState: PokemonState) {
  }
  public error$(): Observable<string> {
    return this.pokemonState.getError$();
  }
  public pokemons$(): Observable<Pokemon[]> {
    return this.pokemonState.getPokemons$();
  }
  // Returns the list of pokemon, with its url to their avatar picture. The results is set as state
  public getPokemonsPagination(page: number): void {
    this.fetchApi.getPokemonsPagination$(page)
      .pipe(
        map((response: PokemonResponseModel) => {
          if (response.results.length === 0){
            throw Error('No content fetched from API');
          }
          return response.results.map(pokemon => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url)
          }));
        })
      )
      .subscribe((pokemons: Pokemon[]) => {
      this.pokemonState.setPokemons$(pokemons);
    }, (error: HttpErrorResponse) => {
      this.pokemonState.setError(error.message);
    });
  }
  // Metohod that handles the fetching of the avatar picture.
  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
}
