import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Pokemon} from '../models/pokemon.model';
import {PokemonResponseModel} from '../models/pokemon-response.model';
import {map} from 'rxjs/operators';

const {pokemonBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})

export class PokemonApi {
  public pokemon: Pokemon;

  constructor(private readonly http: HttpClient) {
  }
  // Fetches one pokemon, by name
  public getPokemon$(name: string): void{
    this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${name}`)
      .pipe(
        map((pokemon: Pokemon) => ({
          ...pokemon,
          image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`
        }))
      )
      .subscribe((pokemon: Pokemon) => {
        this.pokemon = pokemon;
        console.log(pokemon);
      });
  }
  // Fetches 20 pokemon based on page (for pagination).
  public getPokemonsPagination$(page: number): Observable<PokemonResponseModel>{
    return this.http.get<PokemonResponseModel>(`https://pokeapi.co/api/v2/pokemon/?limit=20&offset=${(page - 1) * 20}`);
  }
}
