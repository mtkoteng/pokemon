import {Component} from '@angular/core';
import {SessionService} from '../../shared/service/session.service';

@Component({
  selector: 'app-navbar-component',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{
  constructor(private readonly sessionService: SessionService) {
  }
  // Check if it is a active session from session service. Is active if username is stored.
  get activeSession(): boolean {
    return this.sessionService.active();
  }
  // Cleares the localStorage when logging out.
  clearStorage(): void {
    localStorage.clear();
  }
}
