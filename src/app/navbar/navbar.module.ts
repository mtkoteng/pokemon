import {NgModule} from '@angular/core';
import {NavbarComponent} from './components/navbar.component';
import {NavbarRoutingModule} from './navbar-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  exports: [
    NavbarComponent
  ],
  declarations: [
    NavbarComponent
  ],
  imports: [
    NavbarRoutingModule,
    CommonModule
  ]
})
export class NavbarModule{}
