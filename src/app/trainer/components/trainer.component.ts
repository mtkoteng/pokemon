import {Component, OnInit} from '@angular/core';
import {AppStorage} from '../../utils/app-storage';
import {Pokemon} from "../../pokemon/models/pokemon.model";

@Component({
  selector: 'app-trainer-component',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent{
  private name: string | null;
  private pokemonsCollected: Pokemon[] | null;
  constructor() {
    this.name = AppStorage.get('username');
    this.pokemonsCollected = AppStorage.get('collected');
  }
  get getName(): string | null {
    return this.name;
  }
  get getPokemonsCollected(): Pokemon[] | null {
    return this.pokemonsCollected;
  }

  public removePokemon(pokemon: Pokemon): void{
    const index = this.pokemonsCollected.indexOf(pokemon);
    if (index > -1) {
      this.pokemonsCollected.splice(index, 1);
    }
    AppStorage.set('collected', this.pokemonsCollected);
  }
}
