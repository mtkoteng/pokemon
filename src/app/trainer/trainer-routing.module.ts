import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TrainerPage} from './pages/trainer-page/trainer.page';
import {DetailPage} from '../detail/pages/detail/detail.page';

const routes: Routes = [
  {
    path: '',
    component: TrainerPage
  },
  {
    path: ':name',
    component: DetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TrainerRoutingModule{}
