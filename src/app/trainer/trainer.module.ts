import {NgModule} from '@angular/core';
import {TrainerPage} from './pages/trainer-page/trainer.page';
import {TrainerRoutingModule} from './trainer-routing.module';
import {TrainerComponent} from './components/trainer.component';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [
      TrainerPage,
      TrainerComponent
    ],
    imports: [
      TrainerRoutingModule,
      CommonModule
    ],

})
export class TrainerModule{}
