# Pokemon
## Contributors
Markus Thomassen Koteng and Jakkris Thongma

## The task
In this assignment, we should develop a pokemon site using Angular and 
data from https://pokeapi.co/. The page application handles a trainer that can
collect pokemons from the API.

## Run
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## The solution
### Collecting data
The data that was collected, was collected with Angular's own HttpClient.
This fetched the data from the API, including name, stats and pictures of the 
pokemons.

### Storing
This application uses LocalStorage to store data. It is used to store
the trainer name, and the collected pokemons for the trainer. The trainer
can collect several pokemons of same type, and are also able to uncollect
them, if wanted. All this stored in the storage.

### Pages
#### Login
If the trainer is not logged in (not stored in localStorage), then the trainer
will be sent to this page. Here the trainer can enter its name. If it is stored 
(already logged in), then you will be redirected to Catalogue.
#### Trainer
The trainer page is a simple page that prints the trainer name and a
table of collected pokemons, if pokemons are collected. Here a trainer
can click on a button to get more info about each pokemon, and uncollect
pokemons if he want.
#### Catalogue
The catalogue shows 20 pokemons for each site, and the user can navigate
between the sites, with the buttons. The pokemon is displayed inside a card,
with name, image, and buttons for seeing details and collecting them.
#### Pokemon details
The detail page prints out the details of the pokemon selected. Also here
a trainer can collect the pokemons.
